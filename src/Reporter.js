import React from "react";


const Reporter = ({name, children}) => { 
    return (
    <div className="comp">
        <p>{name}: {children}</p>
    </div>
    );
}

  export default Reporter;
  
